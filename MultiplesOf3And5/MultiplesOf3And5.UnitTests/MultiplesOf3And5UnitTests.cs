﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MultiplesOf3And5;

namespace MultiplesOf3And5.UnitTests
{
    [TestClass]
    public class MultiplesOf3And5UnitTests
    {
        /// <summary>
        /// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
        /// </summary>
        [TestMethod]
        public void SumMultipleOf3And5Below10_Is23()
        {
            var numbers = MultipleOfFactory.MakeSequence(1, 9);

            var sum = numbers.GetMultiplesOf3And5().Sum();

            Assert.AreEqual(23, sum);
        }

        [TestMethod]
        public void NumberIsOtherNumber_1()
        {
            var numbers = MultipleOfFactory.MakeNumber(1);
            Assert.IsInstanceOfType(numbers, typeof(OtherNumber));
        }

        [TestMethod]
        public void NumberIsMultipleOf3_3()
        {
            var numbers = MultipleOfFactory.MakeNumber(3);
            Assert.IsInstanceOfType(numbers, typeof(MultipleOf3));
        }

        [TestMethod]
        public void NumberIsMultipleOf5_5()
        {
            var numbers = MultipleOfFactory.MakeNumber(5);
            Assert.IsInstanceOfType(numbers, typeof(MultipleOf5));
        }

        [TestMethod]
        public void NumberIsMultipleOf3And5_15()
        {
            var numbers = MultipleOfFactory.MakeNumber(15);
            Assert.IsInstanceOfType(numbers, typeof(MultipleOf3And5));
        }


        [TestMethod]
        public void SevenIsNotAMultipleOf3_CantConstruct_ThrowsException()
        {
            Action creatingInvalidMultipleOf3 = () => new MultipleOf3(7);

            Assert.ThrowsException<ArgumentException>(creatingInvalidMultipleOf3);
        }

    }
}

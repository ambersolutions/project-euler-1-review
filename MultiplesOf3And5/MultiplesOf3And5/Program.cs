﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;

namespace MultiplesOf3And5
{
    /// <summary>
    /// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
    /// Find the sum of all the multiples of 3 or 5 below 1000.
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            var numbers = MultipleOfFactory.MakeSequence(1, 999);

            var sum = numbers.GetMultiplesOf3And5().Sum();

            Console.WriteLine("sum of all the multiples of 3 or 5 below 1000 = {0}", sum);
            Console.Read();
        }
    }

    public static class MultipleOfFactory
    {
        public static MultipleOf MakeNumber(int value)
        {
            int remainder3 = value % 3;
            bool isMultipleOf3 = (remainder3 == 0);
            int remainder5 = value % 5;
            bool isMultipleOf5 = (remainder5 == 0);

            if (isMultipleOf3 && isMultipleOf5)
                return new MultipleOf3And5(value);
            else if (isMultipleOf3)
                return new MultipleOf3(value);
            else if (isMultipleOf5)
                return new MultipleOf5(value);
            else
                return new OtherNumber(value);
        }

        public static Sequence MakeSequence(int fromValue, int toValue)
        {
            var numbers = new Sequence();
            for (int n = fromValue; n <= toValue; n++)
            {
                numbers.Add(MakeNumber(n));
            }
            return numbers;
        }
    }

    public abstract class MultipleOf
    {
        protected MultipleOf(int value)
        {
            Value = value;
        }

        public int Value { get; }

        public bool IsMultiplesOf3And5 => IsMultiplesOf3 || IsMultiplesOf5;
        public virtual bool IsMultiplesOf3 { get; }
        public virtual bool IsMultiplesOf5 { get; }

        protected bool IsMultipleOf(int value, int multipleOf)
        {
            int remainder = value % multipleOf;
            return (remainder == 0);
        }
    }

    public class MultipleOf3 : MultipleOf
    {
        public MultipleOf3(int value) : base(value)
        {
            if (!IsMultipleOf(value, 3)) throw new ArgumentException("Value must be a multiple of 3");
        }

        public override bool IsMultiplesOf3 => true;
    }

    public class MultipleOf5 : MultipleOf
    {
        public MultipleOf5(int value) : base(value)
        {
            if (!IsMultipleOf(value, 5)) throw new ArgumentException("Value must be a multiple of 5");
        }

        public override bool IsMultiplesOf5 => true;
    }

    public class MultipleOf3And5 : MultipleOf
    {
        public MultipleOf3And5(int value) : base(value)
        {

            bool isMultipleOf3 = IsMultipleOf(value, 3);
            bool isMultipleOf5 = IsMultipleOf(value, 5);

            if (!isMultipleOf3 && !isMultipleOf5) throw new ArgumentException("Value must be a multiple of 3 or 5");
        }

        public override bool IsMultiplesOf3 => true;
        public override bool IsMultiplesOf5 => true;
    }

    public class OtherNumber : MultipleOf
    {
        public OtherNumber(int value) : base(value) { }
    }

    public class Sequence : List<MultipleOf>
    {
        public Sequence GetMultiplesOf3And5()
        {
            var subset = new Sequence();
            subset.AddRange(this.Where(n => n.IsMultiplesOf3And5));
            return subset;
        }

        public int Sum() => this.Sum(n => n.Value);
    }
}
